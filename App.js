/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {AppState, SafeAreaView, StyleSheet} from 'react-native';
import 'react-native-gesture-handler';
import MainNavigation from './src/navigations/MainNavigation';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      appState: AppState.currentState,
    };
  }

  render() {
    return (
      <>
        <SafeAreaView style={{width: '100%', height: '100%'}}>
          <MainNavigation />
        </SafeAreaView>
      </>
    );
  }
}

const Styles = StyleSheet.create({
  preview: {
    height: 0.1,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
