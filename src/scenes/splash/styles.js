import {StyleSheet} from 'react-native';
import {Colors} from '../../theme';

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.WHITE,
  },
  Image: {
    width: '100%',
    height: '100%',
  },
});

export default Styles;
