import React, {useEffect} from 'react';
import {Image, StatusBar, View} from 'react-native';
import Styles from './styles';
import Assets from '../../assets';
import {Colors} from '../../theme';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    StatusBar.setBarStyle('light-content');
    StatusBar.setBackgroundColor(Colors.BLACK);
    setTimeout(() => {
      navigation.replace('Home');
    }, 3000);
  });

  return (
    <View style={Styles.container}>
      <Image source={Assets.Splash} resizeMode="cover" style={Styles.Image} />
    </View>
  );
};

export default SplashScreen;
