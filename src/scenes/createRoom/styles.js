import {StyleSheet} from 'react-native';
import {Colors} from '../../theme';

const Styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.WHITE,
    flex: 1,
  },
  HeaderView: {
    justifyContent: 'center',
    height: 50,
    borderBottomWidth: 0.5,
  },
  viewDropDown: {
    paddingHorizontal: '5%',
    paddingBottom: 150,
    backgroundColor: Colors.WHITE,
  },
  divider: {
    height: 100,
    width: '100%',
    backgroundColor: 'red',
  },
});

export default Styles;
