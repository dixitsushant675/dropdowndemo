import React, {createRef, useEffect, useState} from 'react';
import {FlatList, SafeAreaView, View} from 'react-native';
import CreateRoomForm from '../../components/CreateRoomForm';
import MyHeader from '../../components/MyHeader';
import createroom from '../../constants/createroom.json';
import Styles from './styles';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const CreateRoom = ({navigation}) => {
  const [arrFilter, setArrFilter] = useState();
  const flatListRef = createRef();
  useEffect(() => {
    let DemoArrFilter = createroom.map(item => {
      return item.field_type === 'dropdown_field'
        ? {
            ...item,
            dropDown: createRef(),
            value: '',
          }
        : {
            ...item,
            value: '',
          };
    });

    setArrFilter(DemoArrFilter);
  }, []);

  const onselectItemIndex = (indexDrop, indexItem) => {
    let Arr = arrFilter.map((item, mapIndex) => {
      return mapIndex === indexDrop
        ? {
            ...item,
            value: indexItem,
          }
        : item;
    });

    setArrFilter(Arr);
  };
  const onSave = () => {
    let filterData = arrFilter.map((item, index) => {
      return {
        field_type: item.field_type,
        value: item.value,
      };
    });

    console.log('On Save Data', filterData);
  };

  return (
    <SafeAreaView style={Styles.container}>
      <MyHeader onPress={onSave} onCancel={() => navigation.goBack()} />
      <KeyboardAwareScrollView>
        <View style={Styles.viewDropDown}>
          <FlatList
            ref={flatListRef}
            showsVerticalScrollIndicator={false}
            data={arrFilter}
            onScrollToIndexFailed={({index, averageItemLength}) => {
              flatListRef.current?.scrollToOffset({
                offset: index * averageItemLength,
                animated: true,
              });
            }}
            renderItem={({item, index}) => (
              <CreateRoomForm
                onselectItemIndex={(index, option) =>
                  onselectItemIndex(index, option)
                }
                item={item}
                index={index}
              />
            )}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default CreateRoom;
