import React, {createRef, useState} from 'react';
import {Pressable, SafeAreaView, ScrollView, Text, View} from 'react-native';
import {Icon} from 'react-native-elements';
import CustomDropDown from '../../components/CustomDropDown';
import MyButton from '../../components/MyButton';
import {default as String, default as Strings} from '../../constants/Strings';
import {Colors} from '../../theme';
import styles from './styles';

const HomeScreen = ({navigation}) => {
  const [Arr, setArr] = useState([
    {
      id: 1,
      officeLocationLabel: 'Office Location',
      officeLocation: '',
      areaLabel: 'Area i care about (for temperature)',
      area: '',
      isAdditional: false,
      dropDownOfficeLocation: false,
      dropDownArea: false,
      dropDown: createRef(),
      dropDown2: createRef(),
      data: String.home.dropDownArr,
      dataArea: String.home.dropDownArr,
    },
  ]);

  const onselectItemIndex = (indexDrop, indexItem, toChange) => {
    let DemoArr = Arr.map((item, mapIndex) => {
      return mapIndex === indexDrop
        ? {
            ...item,
            [toChange]: indexItem,
          }
        : item;
    });

    setArr(DemoArr);
  };

  const addOtherLocation = () => {
    let value = {
      id: Arr.length + 1,
      officeLocationLabel: 'Office Location',
      officeLocation: '',
      areaLabel: 'Area i care about (for temperature)',
      area: '',
      isAdditional: true,
      dropDownOfficeLocation: false,
      dropDownArea: false,
      dropDown: createRef(),
      dropDown2: createRef(),
      data: String.home.dropDownArr,
      dataArea: String.home.dropDownArr,
    };

    let DemoArr = [...Arr, value];

    setArr(DemoArr);
  };

  const onSubmit = () => {
    let filterData = Arr.map((item, index) => {
      return {
        officeLocation: item.officeLocation,
        area: item.area,
        isAdditional: item.isAdditional,
      };
    });

    console.log('filterData', filterData);

    navigation.navigate('CreateRoom');
  };

  return (
    <SafeAreaView style={styles.mainContainer}>
      <ScrollView>
        <View style={styles.container}>
          <Text style={styles.welcomeStyle}>{Strings.home.welcome}</Text>
          <Text style={styles.welcomeDesStyle}>{Strings.home.welcomeDes}</Text>

          {Arr.map((item, index) => {
            return (
              <View style={styles.viewDropDown}>
                <CustomDropDown
                  item={item}
                  label={item.officeLocationLabel}
                  value={item.officeLocation}
                  index={index}
                  ref={item.dropDown}
                  onselectItemIndex={(index, option) =>
                    onselectItemIndex(index, option, 'officeLocation')
                  }
                />
                <CustomDropDown
                  item={item}
                  label={item.areaLabel}
                  value={item.area}
                  index={index}
                  ref={item.dropDown2}
                  onselectItemIndex={(index, option) =>
                    onselectItemIndex(index, option, 'area')
                  }
                />
                {/* {this.state.additional === 1 && !item.isAdditional ? (
                    <Text>{String.home.AdditionalLocation}</Text>
                  ) : null} */}
              </View>
            );
          })}

          <Pressable style={styles.addStyle} onPress={addOtherLocation}>
            <View>
              <Icon
                color={Colors.THEME_VIOLET}
                type="antdesign"
                name="pluscircle"
              />
            </View>
            <Text style={styles.TxtLocation}>
              {Strings.home.addOtherLocation}
            </Text>
          </Pressable>

          <MyButton
            action={onSubmit}
            text={Strings.home.next}
            style={styles.btnStyle}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
export default HomeScreen;
