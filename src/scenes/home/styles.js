import {StyleSheet, Platform} from 'react-native';
import {Colors, FontSizes} from '../../theme';

const Styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: Colors.WHITE,
    flex: 1,
  },
  container: {
    paddingHorizontal: '5%',
    paddingVertical: '10%',
    paddingTop: 80,
    backgroundColor: Colors.WHITE,
  },
  flatListStyle: {
    marginBottom: 50,
  },
  welcomeStyle: {
    fontSize: FontSizes.FONT_SIZE_SUPREME,
    alignSelf: 'center',
  },
  welcomeDesStyle: {
    fontSize: FontSizes.FONT_SIZE_SMALL,
    alignSelf: 'center',
    textAlign: 'center',
    marginVertical: 40,
    paddingHorizontal: 40,
    opacity: 0.6,
  },
  addStyle: {
    flexDirection: 'row',
    marginTop: 50,
    alignItems: 'center',
    width: '90%',
    alignSelf: 'center',
  },
  dropDownContainer: {
    width: 300,
    backgroundColor: 'red',
    // alignSelf: 'center',
    // height: Platform.OS === 'ios' ? 40 : 50,
    // marginVertical: 10,
    // borderBottomWidth: 1,
  },
  dropDownStyle: {
    borderColor: 'transparent',
    borderBottomColor: Colors.BLACK,
  },
  dropdown: {
    alignSelf: 'center',
    backgroundColor: Colors.RED,

    borderWidth: 0,
    borderTopWidth: 1,
  },
  placeholderStyle: {
    position: 'absolute',
  },
  dropdowntextInputStyle: {
    // fontFamily: Fonts.type.regular,
    // fontSize: Fonts.size.medium,
    // zIndex: 1,
    color: Colors.lightGray,
    height: Platform.OS === 'ios' ? 40 : 50,
    borderWidth: 0,
    backgroundColor: Colors.lightGray,
    borderRadius: 18,
  },

  dropdownItem: {
    justifyContent: 'flex-start',
    paddingLeft: 10,
  },
  viewDropDown: {
    width: '90%',
    alignSelf: 'center',
  },
  divider: {height: 1, backgroundColor: 'black'},
  TxtLocation: {
    color: Colors.THEME_VIOLET,
    marginLeft: 10,
  },
  btnStyle: {
    backgroundColor: Colors.THEME_VIOLET,
    width: '70%',
    alignSelf: 'center',
    marginTop: 50,
  },
});

export default Styles;
