const String = {
  home: {
    welcome: 'Welcome!',
    welcomeDes:
      "What temperature matters most? Please share more on what area's temperature matters most to you",

    officeLocation: 'Office Location',
    areaICare: 'Area I care about(for temperature)',
    addOtherLocation: 'Add other Location',

    next: 'NEXT',

    dropDownArr: [
      '10 Hudson Yards Floor 27',
      '307 Hudson Yards Floor 27',
      '307 Hudson Yards Floor 27',
      '307 Hudson Yards Floor 27',
    ],
    AdditionalLocation: 'ADDITIONAL LOCATION',
  },

  createRoom: {
    cancel: 'Cancel',
    creatRoom: 'Create Room',
    save: 'Save',
  },
};

export default String;
