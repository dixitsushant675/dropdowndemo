import React from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Card} from 'react-native-elements';
import {FontSizes, Colors, Fonts, Layout} from '../theme';
import {TextInput} from 'react-native-paper';

function TextInputTitle({
  title,
  placeholder,
  value,
  onChangeText,
  keyboardType,
  style,
  editable,
  maxLength,
  multiline,
  index,
}) {
  return (
    <View style={styles.mainview}>
      <Text style={styles.text}>{title}</Text>
      <View style={[styles.mainCard, {height: multiline ? 70 : 45}]}>
        {/* <View style={styles.mainView}> */}
        <TextInput
          mode="flat"
          value={value}
          keyboardType={keyboardType}
          style={[styles.textinput, style]}
          placeholder={placeholder}
          placeholderTextColor={Colors.GRAY}
          onChangeText={val => onChangeText(index, val)}
          textTransform="capitalize"
          multiline={multiline}
          maxLength={maxLength === undefined ? undefined : maxLength}
          editable={editable === undefined ? true : false}
        />
        {/* </View> */}
      </View>
      {multiline ? (
        <Text style={styles.textLength}>{`${value.length}/ ${maxLength}`}</Text>
      ) : null}
    </View>
  );
}

const styles = StyleSheet.create({
  mainview: {
    // width: Layout.SCREEN_WIDTH * 0.9,
    marginTop: 25,
  },
  mainCard: {
    width: '100%',
    height: 45,
    alignItems: 'center',
    backgroundColor: Colors.WHITE,
    flexDirection: 'row',
    alignSelf: 'center',
    padding: 0,
    marginTop: 0,
    borderWidth: 0,
    // borderBottomWidth: 1,
    // borderBottomColor: Colors.GRAY,
  },
  mainView: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 10,
    borderWidth: 0,
  },
  text: {
    fontSize: FontSizes.FONT_SIZE_DEFAULT,
    color: Colors.GRAY,
  },
  textinput: {
    borderBottomWidth: 0,
    color: Colors.BLACK,
    width: '100%',
    height: 45,
    alignItems: 'center',
    backgroundColor: Colors.WHITE,
    flexDirection: 'row',
    alignSelf: 'center',
    padding: 0,
    marginTop: 0,
    borderWidth: 0,
  },
  textLength: {
    alignSelf: 'flex-end',
    marginTop: 5,
  },
});

export default TextInputTitle;
