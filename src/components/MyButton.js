import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';
import {Icon} from 'react-native-elements';
import {FontSizes, Colors, Fonts} from '../theme';

function MyButton({
  color,
  size,
  styleText,
  text,
  action,
  iconName,
  iconType,
  disabled,
  style,
  fontSize,
  popup,
  retake,
}) {
  return (
    <TouchableOpacity
      style={[
        disabled ? Styles.buttonViewDisable : Styles.buttonView,
        style,
        {marginVertical: popup === undefined ? 15 : 0},
      ]}
      onPress={action}
      disabled={disabled}>
      {iconName === undefined ? null : (
        <Icon
          name={iconName}
          type={iconType}
          size={size === undefined ? 22 : size}
          color={color === undefined ? Colors.WHITE : color}
        />
      )}
      {fontSize === undefined ? (
        <Text style={Styles.buttonText}>
          {text} {retake}
        </Text>
      ) : (
        <Text
          style={[
            Styles.buttonText,
            styleText,
            {fontSize: fontSize, marginHorizontal: 5},
          ]}>
          {text}
        </Text>
      )}
    </TouchableOpacity>
  );
}

const Styles = StyleSheet.create({
  buttonView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.PRIMARY_DARK,
    padding: 10,
    borderRadius: 5,
    marginVertical: 15,
    borderBottomWidth: 0.5,
    borderRightWidth: 0.5,
    height: 40,
    width: '100%',
    borderColor: '#FFFFFF',
  },
  buttonViewDisable: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.GRAY,
    padding: 10,
    borderRadius: 5,
    marginVertical: 15,
    borderBottomWidth: 0.5,
    borderRightWidth: 0.5,
    height: 40,
    width: '100%',
    borderColor: '#FFFFFF',
  },
  buttonText: {
    color: Colors.THEME_WHITE,
    marginHorizontal: 15,
    textAlign: 'center',
  },
  buttonTextDisable: {
    color: Colors.THEME_WHITE,
    marginHorizontal: 15,
  },
});

export default MyButton;
