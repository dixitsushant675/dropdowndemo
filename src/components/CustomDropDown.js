import React, {forwardRef} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import {Colors} from '../theme';
import {TextInput} from 'react-native-paper';
import {Icon} from 'react-native-elements';

function CustomDropDown(
  {item, label, index, onselectItemIndex, value, borderBottomColor},
  ref,
) {
  let Arrow = 'arrow-drop-down';
  return (
    <ModalDropdown
      ref={ref}
      // onDropdownWillHide={() => {
      //   Arrow = 'arrow-drop-down';
      // }}
      // onDropdownWillShow={() => {
      //   Arrow = 'arrow-drop-up';
      // }}
      showsVerticalScrollIndicator={false}
      dropdownStyle={styles.dropdownStyle}
      dropdownTextStyle={styles.dropdownTextStyle}
      onSelect={(indexDrop, option) => onselectItemIndex(index, option)}
      style={[styles.DefaultStyle, {borderBottomColor: borderBottomColor}]}
      options={item.data}>
      <View style={styles.viewStyle}>
        <TouchableOpacity
          style={styles.touchStyle}
          onPress={() => {
            ref.current.show();
            // if (value === '') {
            //   onselectItemIndex(
            //     index,
            //     ' ',
            //     label === 'Office Location' ? 'officeLocation' : 'area',
            //   );
            // }
          }}>
          <TextInput
            disabled={true}
            value={value}
            style={styles.textinputStyle}
            label={label}
          />
        </TouchableOpacity>
        <Icon type="materialicons" name={Arrow} />
      </View>
    </ModalDropdown>
  );
}

const styles = StyleSheet.create({
  touchStyle: {
    width: '90%',
  },
  viewStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  dropdownStyle: {
    width: '90%',
  },
  dropdownTextStyle: {
    paddingVertical: 20,
    color: Colors.BLACK,
  },
  DefaultStyle: {
    borderBottomWidth: 1,
    borderColor: Colors.THEME_VIOLET,
    marginTop: 20,
  },
  textinputStyle: {backgroundColor: 'transparent'},
});

export default forwardRef(CustomDropDown);
