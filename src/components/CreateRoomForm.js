import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Colors} from '../theme';
import CustomDropDown from './CustomDropDown';
import TextInputTitle from './TextInputTitle';

function CreateRoomForm({item, index, onselectItemIndex}) {
  return (
    <View style={styles.mainview}>
      {item.field_type === 'textfield' ? (
        <TextInputTitle
          placeholder={item.placeholder}
          maxLength={item.textLength}
          value={item.value}
          onChangeText={onselectItemIndex}
          index={index}
        />
      ) : null}
      {item.field_type === 'multiLine_textfield' ? (
        <TextInputTitle
          title={item.placeholder}
          maxLength={item.textLength}
          multiline={true}
          value={item.value}
          onChangeText={onselectItemIndex}
          index={index}
        />
      ) : null}
      {item.field_type === 'dropdown_field' ? (
        <CustomDropDown
          onselectItemIndex={onselectItemIndex}
          item={item}
          ref={item.dropDown}
          value={item.value}
          label={item.placeholder}
          index={index}
          borderBottomColor={Colors.GRAY}
        />
      ) : null}
      {/* {item.field_type === "radio_button" ? <TextInputTitle /> : null} */}
    </View>
  );
}

const styles = StyleSheet.create({
  mainview: {
    // width: Layout.SCREEN_WIDTH * 0.9,
  },
});

export default CreateRoomForm;
