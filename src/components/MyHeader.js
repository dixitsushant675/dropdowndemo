import React from 'react';
import {StyleSheet, Text, TouchableOpacity, Pressable} from 'react-native';
import {Header, Icon} from 'react-native-elements';
import String from '../constants/Strings';
import {FontSizes, Colors, Fonts} from '../theme';

function MyHeader({onPress, onCancel}) {
  return (
    <Header
      statusBarProps={{translucent: true}}
      backgroundColor={Colors.WHITE}
      containerStyle={styles.container}
      leftComponent={
        <Pressable onPress={onCancel}>
          <Text>{String.createRoom.cancel.toUpperCase()}</Text>
        </Pressable>
      }
      centerComponent={<Text>{String.createRoom.creatRoom.toUpperCase()}</Text>}
      rightComponent={
        <Pressable onPress={onPress}>
          <Text>{String.createRoom.save.toUpperCase()}</Text>
        </Pressable>
      }
    />
  );
}

const styles = StyleSheet.create({
  container: {},
  menuOption: {
    width: 20,
    height: 20,
    resizeMode: 'contain',
  },
});

export default MyHeader;
