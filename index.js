/**
 * @format
 */
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
// Imports: Dependencies
import React from 'react';
// Imports: Screens
import App from './App';
import 'react-native-gesture-handler';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {Colors} from './src/theme';

const theme = {
  ...DefaultTheme,
  dark: true,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.THEME_VIOLET,
  },
};

export default function Main() {
  return (
    <PaperProvider theme={theme}>
      <App />
    </PaperProvider>
  );
}

AppRegistry.registerComponent(appName, () => Main);
